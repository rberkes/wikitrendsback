#!/usr/bin/python
# coding: utf-8
"""The master.py file downloads the hourly log file from wikipedia,
    parses it, and adds the hourly hit data to a MongoDB server instance.
    """
import urllib.request, urllib.error, urllib.parse
import os
import glob
from time import time
import subprocess
import wikicount as wikicount

EMAILOUT = "/wikidata/zEMAIL"
DAY, MONTH, YEAR, HOUR, expiretime = wikicount.fnReturnTimes()
HOUR = wikicount.minusHour(int(HOUR))
HOUR = wikicount.minusHour(int(HOUR))
HOUR = wikicount.minusHour(int(HOUR))
DAY, MONTH, HOUR = wikicount.fnFormatTimes(DAY, MONTH, HOUR)
SYSOUT = '/wikidata/sysout'
FILEBASEGZ = "/wikidata/staging/pagecounts.wikidata.gz"
BASEGZ=".wikidata.gz"
FILEBASE = "/wikidata/staging/pagecounts"
FFILE = 'q_pagecounts.'+str(HOUR)

def init_newdirs(lang, FILENAME):
    outfilename = FILENAME.replace('staging', 'ondeck')
    imgfilename = FILENAME.replace('staging', 'image')
    catFileName = FILENAME.replace('staging', 'category')
    try:
        OFILE = open(outfilename, "w")
    except IOError:
        ODIR = '/wikidata/' + str(lang) + '_ondeck'
        if not os.path.exists(ODIR):
            print("create new for " + ODIR)
            os.makedirs(ODIR)
        outfilename = FILENAME.replace('staging', 'ondeck')
        OFILE = open(outfilename, "w")
    try:
        catFile = open(catFileName, "a")
    except IOError:
        CATDIR = '/wikidata/' + str(lang) + '_category/'
        if not os.path.exists(CATDIR):
            print("create new dir for " + CATDIR)
            os.makedirs(CATDIR)
        catFileName = FILENAME.replace('staging', 'category')
        catFile = open(catFileName, "a")
    try:
        imgFile = open(imgfilename, "a")
    except IOError:
        imgDir = '/wikidata/' + str(lang) + '_image'
        if not os.path.exists(imgDir):
            print("create new dir for " + imgDir)
            os.makedirs(imgDir)
        imgfilename = FILENAME.replace('staging', 'image')
        imgFile = open(imgfilename, "a")
    return OFILE, catFile, imgFile

def filter_records():
    categoryList=['Category:', 'Категория']
    FILELIST=['Image:', 'File:']
    UNWATCHED=['Template:', 'Wikipedia_talk:', 'Category_talk',
               'talk', '.php']
    LANG_DICT=wikicount.getLangDict()
    for key,lang in LANG_DICT.items():
        a = time()
        RECS = 0
        RECERRS = 0
        for FILENAME in glob.glob('/wikidata/' + str(lang) + '_staging/q*'):
            OFILE,catFile,imgFile=init_newdirs(lang,FILENAME)
            IFILE = open(FILENAME, "r")
            for line in IFILE:
                record = line.strip().split()
                try:
                    if record[1] in categoryList:
                        catFile.write(line)
                        RECS += 1
                    elif record[1] in FILELIST:
                        imgFile.write(line)
                        RECS += 1
                    elif record[1] not in UNWATCHED:
                        OFILE.write(line)
                        RECS += 1
                except IndexError:
                    continue
            OFILE.close()
            imgFile.close()
            catFile.close()
            IFILE.close()
            os.remove(FILENAME)
        b = time()
        c = b - a
        d = round(c, 3)
        SYSFILE = open(SYSOUT, 'a')
        SYSFILE.write(
            "[master.py][p2_filter] Lang: " + str(lang) + " runtime: " + 
        str(d) + " seconds. Recs written: " + 
            str(RECS) + " Record errors: " + str(RECERRS)+"\n")
        SYSFILE.close()


def p15_split_by_language():
    tc=0
    print(FILEBASE)
    IFILE = open(FILEBASE, "r",encoding="utf-8")
    for line in IFILE:
        record = line.strip().split()
        try:
            LANGUAGEUSED = record[0].lower()
        except IndexError:
            print(record)
            continue
        Fdir = '/wikidata/' + str(LANGUAGEUSED) + '_staging/'
        fName = Fdir+FFILE
        try:
            fp = open(fName, "a",encoding="utf-8")
        except IOError:
            if not os.path.exists(Fdir):
                os.makedirs(Fdir)
            fp = open(fName, "w",encoding="utf-8")
        tc+=1
        fp.write(str(line))
        fp.close()
    IFILE.close()
    print('Total Count: ',str(tc))
    try:
        os.remove(FILEBASE)
    except OSError:
        pass

print("******... Filtering File by language*****")
p15_split_by_language()
print("*****Unzipped into staging area. ******")
filter_records()
print("*****Done FIltering Records!*****")
