import wikicount
import redis
HOME='/home/ubuntu'
DIR='/wikitrendsback/new_redis/renders/'
NUMARTICLES=50
r=redis.StrictRedis(host='127.0.0.1',port=6379,db=0)

def render(lang,hitlist):
    LANG=str(lang,'utf-8')
    ifp=open(HOME+DIR+str(LANG),'w')
    counter=1
    for article, score in hitlist:
        article = str(article,'utf-8').lstrip('b')
        ifp.write(str(counter)+'#'+str(article)+'#'+str(score)+'\n')
        counter+=1
    return 

def get_top(lang):
    qry = r.zrevrange(lang,0,NUMARTICLES-1,withscores=True)
    return qry

LD=wikicount.getLangDict()
for k,v in LD.items():
    hits = get_top(k)
    render(v,hits)
#print(LD)
