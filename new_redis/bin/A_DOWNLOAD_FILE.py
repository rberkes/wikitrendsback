#!/usr/bin/python
# coding: utf-8
"""The master.py file downloads the hourly log file from wikipedia,
    parses it, and adds the hourly hit data to a MongoDB server instance.
    """
import urllib.request, urllib.error, urllib.parse
import os
import glob
from time import time
import subprocess
import wikicount as wikicount

EMAILOUT = "/wikidata/zEMAIL"
DAY, MONTH, YEAR, HOUR, expiretime = wikicount.fnReturnTimes()
HOUR = wikicount.minusHour(int(HOUR))
HOUR = wikicount.minusHour(int(HOUR))
HOUR = wikicount.minusHour(int(HOUR))
DAY, MONTH, HOUR = wikicount.fnFormatTimes(DAY, MONTH, HOUR)
SYSOUT = '/wikidata/sysout'
FILEBASEGZ = "/wikidata/staging/pagecounts.wikidata.gz"
BASEGZ = ".wikidata.gz"
FILEBASE = "/wikidata/staging/pagecounts"
FFILE = 'q_pagecounts.'+str(HOUR)

def p0_dl():
    """
    Does the dl from dumps.wikimedia.org.  
    """
    url = "http://dumps.wikimedia.org/other/pageviews/" + str(YEAR) + "/" + str(YEAR) + "-" + str(
        MONTH) + "/pageviews-"
    urldate = str(YEAR) + str(MONTH) + str(DAY)
    urlSUFFIX = "-" + str(HOUR) + "*.gz"
    url += urldate
    url += urlSUFFIX
    SYSFILE = open(SYSOUT, 'w')
    a = time()
    url = "http://dumps.wikimedia.org/other/pageviews/" + str(YEAR) + "/" + str(YEAR) + "-" + str(
            MONTH) + "/pageviews-"
    urldate = str(YEAR) + str(MONTH) + str(DAY)
    urlSUFFIX = "-" + str(HOUR) + "0000.gz" 
    url += urldate
    url += urlSUFFIX
    try:
        COUNTFILE = urllib.request.urlretrieve(url,FILEBASEGZ)
        b = time()
        c = b - a
        d = round(c, 3)
        SYSFILE.write("[master.py][p0_dl] Successful download of " + str(url) + " in " + str(d) + " seconds.\n")
    except urllib.error.HTTPError as e:
        SYSFILE.write("[master.py][p0_dl] 404 not found error\n")
        print(e.fp.read())

def p1_unzip_download():
    stdo = open(FILEBASE,'w')
    stde = open(SYSOUT,'a')
    proc = subprocess.Popen(['zcat',FILEBASEGZ],stdout=stdo,stderr=stde)
    proc.wait()
#    subprocess.call(['zcat',FILEBASE])
    stdo.close()
    stde.close()


print("******DOWNLOADING Log file for Hour: "+str(HOUR)+" *******")
p0_dl()
print("******FILE Downloaded. Now unzipping...")
p1_unzip_download()
