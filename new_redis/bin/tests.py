import unittest
import os
import redis
class TestStuff(unittest.TestCase):
    def test_paths(self):
        self.assertTrue(os.path.exists('/wikidata'))

    def test_redis(self):
        r = redis.Redis(host='172.17.0.2',port=6379,db=0)
        y = r.client_list()
        self.assertEqual(y[0]['cmd'],'client')

if __name__ == '__main__':
    unittest.main()



