#/usr/bin/python
# -*- coding: utf-8 -*-
"""The master.py file downloads the hourly log file from wikipedia,
    parses it, and adds the hourly hit data to a MongoDB server instance.
    """
import os
import glob
import time
import redis
from time import time
from datetime import datetime
import wikicount as wikicount
HOSTBOX = '127.0.0.1'
#r = redis.StrictRedis(host='localhost',port=6379,db=0)

#LANGUAGE_PROCESS_LIST = [ 'en','de','ru']
SYSOUT = "/wikidata/zSYSOUT"
FILEBASE = "/wikidata/staging/pagecounts.wikidata"
DAY, MONTH, YEAR, HOUR, EXPIRETIME = wikicount.fnReturnTimes()
HOUR = wikicount.minusHour(int(HOUR))
HOUR = wikicount.minusHour(int(HOUR))
HOUR = wikicount.minusHour(int(HOUR))
DAY, MONTH, HOUR = wikicount.fnFormatTimes(DAY, MONTH, HOUR)
TOTAL_RECORDS_UPDATED = 0
RECDIVIDE = 1000
current_time = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
HOMEDIR = os.path.expanduser('~')
#schema_path = WD+"record.avro"
#FP_DICT = { 0: '',1:'',2:'',3:''}
LANG_DICT= wikicount.getLangDict()
SYSFILE = open(SYSOUT,'a')
#def get_number(d,val):
#    FOUND=False
#    for k,v in d.items():
#        if bytes(v) == val:
#            FOUND=True
#            return k
#    if not FOUND:
#        return 9999
r = redis.Redis(host=HOSTBOX,port=6379,db=0)
#for k,v in LANG_DICT.items():
#    FP_DICT[v] = redis.Redis(host='localhost',port=6379,db=v)
for k,v in LANG_DICT.items():
    records = 0
    count = 0
    PATH='/wikidata/'+str(v,'utf-8')+'_staging/q_p'
#    print('now processing '+str(PATH)+'...')
    for filename in glob.glob('/wikidata/' + str(v,'utf-8').lstrip('b') + '_staging/q_p*.*'):
#        print('grabbed file '+str(filename))
        count+=1
        records = 0
        ifp = open(filename,'r')
        start = time()
        reclist = []
        for line in ifp:
            line = line.strip().split()
            lang = line[0]
            try:
                title = line[1]
            except:
                continue
            #replace comma in title with _ ; db transforms use csv's
            title = title.replace(",","_")
            try:
                hits = line[2]
            except IndexError:
                continue
            value = hits
            try:
                r.zincrby(k,title,value)
            except redis.exceptions.ResponseError:
                print(str(value))
                continue
            records += 1
        end=time()
        try:
          print("Processing: Language: "+str(v)+" Records: "+str(records)+" Total time(secs): "+str(end-start))
          SYSFILE.write('Language: '+str(v)+' Records: '+str(records)+' Time: '+str(end-start)+' seconds.\n')
        except:
          print("error processing: "+str(v))
          SYSFILE.write('error processing: '+str(v)+'\n')
     
        ifp.close()
        try:
          os.remove(filename)
        except FileNotFoundError:
          print('could not delete '+str(filename))
          continue


SYSFILE.close()

