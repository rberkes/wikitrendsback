#coding: utf-8
from datetime import date
import datetime
import time
import glob
import redis
import re
from wsgiref.handlers import format_date_time
REDISHOST='127.0.0.1'
timeZoneHours = 9
def flushvalues():
    r = redis.Redis(host=REDISHOST,port=6379,db=0)
    r.flushdb()
    return
def setLangDB():
    LANG_DICT = {}
    count=0
    r = redis.Redis(host=REDISHOST,port=6379,db=9)
    r.flushdb()
    for p in glob.glob('/wikidata/*_staging'):
        s = re.search('/wikidata/(\S+)_staging',p)
        print(s.group(1))
        LANG_DICT[count] = str(s.group(1))
        count+=1

    for k,v in LANG_DICT.items():
        r.set(k,v)
    return LANG_DICT

def getLangDict():
    LANG_DICT = {}
    count=0
    r = redis.Redis(host=REDISHOST,port=6379,db=9)
    while r.get(count):
        LANG_DICT[count]=r.get(count)
 #   print(s.group(1))
        count+=1

    return LANG_DICT

def fnReturnTimes():
    TODAY = date.today()
    YEAR = TODAY.year
    DAY = TODAY.day
    MONTH = TODAY.month
    HOUR = time.strftime('%H')
    now = datetime.datetime.now()
    half = now+datetime.timedelta(minutes=45)
    stamp = time.mktime(half.timetuple())
    expiretime = format_date_time(stamp)
    if int(HOUR) < 8:
        DAY -= 1
    if DAY == 0:
        DAY = 30
        MONTH -= 1
    if MONTH == 0:
        DAY = 31
        MONTH = 12
        YEAR -= 1
    return DAY, MONTH, YEAR, HOUR, expiretime

def fnFormatTimes(DAY, MONTH, HOUR):
    HOUR = '%02d' % (int(HOUR),)
    DAY = '%02d' % (int(DAY),)
    MONTH = '%02d' % (int(MONTH),)
    return DAY, MONTH, HOUR

def minusHour(HOUR):
    for x in range(timeZoneHours):
        HOUR -= 1
        if HOUR == -1:
            HOUR = 23
        elif HOUR == -2:
            HOUR = 22
        elif HOUR == -3:
            HOUR = 21
        elif HOUR == -4:
            HOUR = 20
        elif HOUR == -5:
            HOUR = 19
        elif HOUR == -6:
            HOUR = 18
        elif HOUR == -7:
            HOUR = 17
    return HOUR

