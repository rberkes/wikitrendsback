import hug
import os
@hug.get('/render/{lang}')
def render(lang: str):
    LANG=str(lang,'utf-8')
    stream='<h3>Top '+str(lang,'utf-8')+' Articles for Today</h3><ol>'
    ifp=open(os.path.abspath('en'),'r')
    for line in ifp:
        line=line.strip().split('#')
        stream+='<li>'+str(line[1])+'\t\tHits: '+str(line[2])+'</li>'
    stream+='</ol>'
    return stream
@hug.get(output=hug.output_format.html)
def en():
    stream='<h3>Top English Articles for Today</h3><ol>'
    ifp=open(os.path.abspath('en'),'r')
    for line in ifp:
        line=line.strip().split('#')
        stream+='<li>'+str(line[1])+'\t\tHits: '+str(line[2])+'</li>'
    stream+='</ol>'
    return stream
@hug.get(output=hug.output_format.html)
def simple():
    stream='<h3>Top Simple English Articles for Today</h3><ol>'
    ifp=open(os.path.abspath('simple'),'r')
    for line in ifp:
        line=line.strip().split('#')
        stream+='<li>'+str(line[1])+'\t\tHits: '+str(line[2])+'</li>'
    stream+='</ol>'
    return stream
