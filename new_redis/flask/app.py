from flask import Flask, g, render_template
app = Flask(__name__)
HOME='/home/ubuntu'

@app.route("/")
def main():
    return "Welcome!"

@app.route("/list/<language>")
def list(language):
    fp = open(HOME+'/wikitrendsback/new_redis/renders/' + str(language), 'r')
    page = '<html>'
    page += '<h2>Top Pages for '+str(language)+'</h2><ol>'
    for line in fp:
        line=line.strip().split('#')
        page+='<li>'+str(line[1])+'  '+str(line[2])+' </li>'
    page+='</ol></html>'
    return page

@app.route("/filtered/<language>")
def filtered(language):
    fp = open(HOME+'/wikitrendsback/new_redis/renders/' + str(language), 'r')
    page = '<html><ol>'
    titleList = [ 'Main_Page', 'Special:Search', '-','404.php' ]
    for line in fp:
        line=line.strip().split('#')
        if line[1] not in titleList:
            page+='<li>'+str(line[1])+' ::> '+str(line[2])+'</li>'
    page+='</ol></html>'
    return page

if __name__ == "__main__":
    app.run()
