from kafka import KafkaConsumer,TopicPartition
import avro.schema
import avro.io
import io
import os

consumer = KafkaConsumer('ru',group_id='ru-group',bootstrap_servers=['localhost:9092'])
consumer.assign(TopicPartition('ru',0))

HD=os.path.expanduser('~')
WD='/Dropbox/Bitbucket/wikitrendsback/new/bin/'
schema_path=HD+WD+'record.avro'

schema=avro.schema.Parse(open(schema_path).read())

for msg in consumer:
    bytes_reader = io.BytesIO(msg.value[5:])
    decoder = avro.io.BinaryDecoder(bytes_reader)
    reader = avro.io.DatumReader(schema)
    logrec = reader.read(decoder)
    print(logrec)
