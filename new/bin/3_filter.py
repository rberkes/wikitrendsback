#!/usr/bin/python
# coding: utf-8
"""The master.py file downloads the hourly log file from wikipedia,
    parses it, and adds the hourly hit data to a MongoDB server instance.
    """
import os
import glob
import re
import string
import time
import wikicount as wikicount
SYSOUT = "/wikidata/zSYSOUT"
FILEBASE = "/wikidata/staging/pagecounts.wikidata"
DAY, MONTH, YEAR, HOUR, EXPIRETIME = wikicount.fnReturnTimes()
HOUR = wikicount.minusHour(int(HOUR))
DAY, MONTH, HOUR = wikicount.fnFormatTimes(DAY, MONTH, HOUR)
LANGLIST = wikicount.LList
SYSOUT = '/wikidata/sysout'
TOTAL_RECORDS_UPDATED = 0
from time import time

def init_newdirs(lang,FILENAME):
    outfilename = FILENAME.replace('staging', 'ondeck')
    imgfilename = FILENAME.replace('staging', 'image')
    CATFILENAME = FILENAME.replace('staging', 'category')
    try:
        OFILE = open(outfilename, "w")
    except IOError:
        ODIR = '/wikidata/' + str(lang) + '_ondeck'
        if not os.path.exists(ODIR):
           print("create new for " + ODIR)
           os.makedirs(ODIR)
        outfilename = FILENAME.replace('staging', 'ondeck')
        OFILE = open(outfilename, "w")
    try:
        CATFILE = open(CATFILENAME, "a")
    except IOError:
        CATDIR = '/wikidata/' + str(lang) + '_category/'
        if not os.path.exists(CATDIR):
           print("create new dir for " + CATDIR)
           os.makedirs(CATDIR)
        CATFILENAME = FILENAME.replace('staging', 'category')
        CATFILE = open(CATFILENAME, "a")
    try:
        IMGFILE = open(imgfilename, "a")
    except IOError:
        IMGDIR = '/wikidata/' + str(lang) + '_image'
        if not os.path.exists(IMGDIR):
           print("create new dir for " + IMGDIR)
           os.makedirs(IMGDIR)
        imgfilename = FILENAME.replace('staging', 'image')
        IMGFILE = open(imgfilename, "a")
    return OFILE, CATFILE, IMGFILE

def filter_records():
    CATEGORYLIST=['Category:', 'Категория']
    FILELIST=['Image:', 'File:']
    UNWATCHED=['Template:', 'Wikipedia_talk:', 'Category_talk',
               'talk', '.php']
    for lang in LANGLIST:
        a = time()
        RECS = 0
        RECERRS = 0
        for FILENAME in glob.glob('/wikidata/' + str(lang) + '_staging/q*'):
            OFILE,CATFILE,IMGFILE=init_newdirs(lang,FILENAME)
            print(FILENAME)
            IFILE = open(FILENAME, "r")
            for line in IFILE:
                record = line.strip().split()
                try:
                  if record[1] in CATEGORYLIST:
                    CATFILE.write(line)
                    RECS += 1
                  elif record[1] in FILELIST:
                    IMGFILE.write(line)
                    RECS += 1
                  elif record[1] not in UNWATCHED:
                    OFILE.write(line)
                    RECS += 1
                except IndexError:
                  continue
            OFILE.close()
            IMGFILE.close()
            CATFILE.close()
            IFILE.close()
            os.remove(FILENAME)
        b = time()
        c = b - a
        d = round(c, 3)
        SYSFILE = open(SYSOUT, 'a')
        SYSFILE.write(
            "[master.py][p2_filter] Lang: " + str(lang) + " runtime: " + 
        str(d) + " seconds. Recs written: " + 
            str(RECS) + " Record errors: " + str(RECERRS))
        SYSFILE.close()

filter_records()
