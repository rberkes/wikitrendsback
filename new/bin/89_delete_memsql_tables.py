import threading
import time
from memsql.common import database
import json
import datetime

TODAY=datetime.datetime.today().strftime('%Y-%m-%d')
HOST="10.138.0.2"
PORT = 3306
USER = "wikicount"
PASSWORD = "wikicount"

DATABASE = "wikitrends"
TABLE = "en"

QUERY_TEXT = "DELETE FROM en;"
EMAILFILE = '/home/rob_berkes/wikitrendsback/new/reports/email_report.'+str(TODAY)

def get_connection(db=DATABASE):
    return database.connect(host=HOST,port=PORT,user=USER,password=PASSWORD,database=db)

conn=get_connection(db='wikitrends');
RETLIST=conn.query(QUERY_TEXT)
QUERY_TEXT = ("ANALYZE TABLE en;")
conn.query(QUERY_TEXT)

LANG='de'
QUERY_TEXT = "DELETE FROM "+str(LANG)+";"
conn=get_connection(db='wikitrends');
RETLIST=conn.query(QUERY_TEXT)
QUERY_TEXT = ("ANALYZE TABLE de;")
conn.query(QUERY_TEXT)

LANG='ru'
QUERY_TEXT = "DELETE FROM "+str(LANG)+";"
conn=get_connection(db='wikitrends');
RETLIST=conn.query(QUERY_TEXT)
QUERY_TEXT = ("ANALYZE TABLE ru;")
conn.query(QUERY_TEXT)

LANG='simple'
QUERY_TEXT = "DELETE FROM "+str(LANG)+";"
conn=get_connection(db='wikitrends');
RETLIST=conn.query(QUERY_TEXT)
QUERY_TEXT = ("ANALYZE TABLE ru;")
conn.query(QUERY_TEXT)
