#!/usr/bin/python
# coding: utf-8
"""The master.py file downloads the hourly log file from wikipedia,
    parses it, and adds the hourly hit data to a MongoDB server instance.
    """
import sys
import urllib.request, urllib.error, urllib.parse
import os
from datetime import date
import wikicount as wikicount
import re
import glob
import string
from time import time
import subprocess

EMAILOUT = "/wikidata/zEMAIL"
FILEBASE = "/wikidata/staging/pagecounts.wikidata.gz"
DAY, MONTH, YEAR, HOUR, expiretime = wikicount.fnReturnTimes()
HOUR = wikicount.minusHour(int(HOUR))
HOUR = wikicount.minusHour(int(HOUR))
HOUR = wikicount.minusHour(int(HOUR))
#HOUR = wikicount.minusHour(int(HOUR))
DAY, MONTH, HOUR = wikicount.fnFormatTimes(DAY, MONTH, HOUR)
LANGLIST = wikicount.LList
SYSOUT = '/wikidata/sysout'
FILEBASEGZ = "/wikidata/staging/pagecounts.wikidata.gz"
BASEGZ=".wikidata.gz"
FILEBASE = "/wikidata/staging/pagecounts"
FFILE = 'q_pagecounts.'+str(HOUR)
PROCESSED_LANGUAGES = ['en', 'de' , 'ru']

def init_newdirs(lang,FILENAME):
    outfilename = FILENAME.replace('staging', 'ondeck')
    imgfilename = FILENAME.replace('staging', 'image')
    CATFILENAME = FILENAME.replace('staging', 'category')
    try:
        OFILE = open(outfilename, "w")
    except IOError:
        ODIR = '/wikidata/' + str(lang) + '_ondeck'
        if not os.path.exists(ODIR):
           print("create new for " + ODIR)
           os.makedirs(ODIR)
        outfilename = FILENAME.replace('staging', 'ondeck')
        OFILE = open(outfilename, "w")
    try:
        CATFILE = open(CATFILENAME, "a")
    except IOError:
        CATDIR = '/wikidata/' + str(lang) + '_category/'
        if not os.path.exists(CATDIR):
           print("create new dir for " + CATDIR)
           os.makedirs(CATDIR)
        CATFILENAME = FILENAME.replace('staging', 'category')
        CATFILE = open(CATFILENAME, "a")
    try:
        IMGFILE = open(imgfilename, "a")
    except IOError:
        IMGDIR = '/wikidata/' + str(lang) + '_image'
        if not os.path.exists(IMGDIR):
           print("create new dir for " + IMGDIR)
           os.makedirs(IMGDIR)
        imgfilename = FILENAME.replace('staging', 'image')
        IMGFILE = open(imgfilename, "a")
    return OFILE, CATFILE, IMGFILE

def filter_records():
    CATEGORYLIST=['Category:', 'Категория']
    FILELIST=['Image:', 'File:']
    UNWATCHED=['Template:', 'Wikipedia_talk:', 'Category_talk',
               'talk', '.php']
    for lang in PROCESSED_LANGUAGES:
        a = time()
        RECS = 0
        RECERRS = 0
        for FILENAME in glob.glob('/wikidata/' + str(lang) + '_staging/q*'):
            OFILE,CATFILE,IMGFILE=init_newdirs(lang,FILENAME)
            print(FILENAME)
            IFILE = open(FILENAME, "r")
            for line in IFILE:
                record = line.strip().split()
                try:
                  if record[1] in CATEGORYLIST:
                    CATFILE.write(line)
                    RECS += 1
                  elif record[1] in FILELIST:
                    IMGFILE.write(line)
                    RECS += 1
                  elif record[1] not in UNWATCHED:
                    OFILE.write(line)
                    RECS += 1
                except IndexError:
                  continue
            OFILE.close()
            IMGFILE.close()
            CATFILE.close()
            IFILE.close()
            os.remove(FILENAME)
        b = time()
        c = b - a
        d = round(c, 3)
        SYSFILE = open(SYSOUT, 'a')
        SYSFILE.write(
            "[master.py][p2_filter] Lang: " + str(lang) + " runtime: " + 
        str(d) + " seconds. Recs written: " + 
            str(RECS) + " Record errors: " + str(RECERRS))
        SYSFILE.close()

def p0_dl():
    """
    Does the dl from dumps.wikimedia.org.  
    """
    url = "http://dumps.wikimedia.org/other/pageviews/" + str(YEAR) + "/" + str(YEAR) + "-" + str(
        MONTH) + "/pageviews-"
    urldate = str(YEAR) + str(MONTH) + str(DAY)
    urlSUFFIX = "-" + str(HOUR) + "*.gz"
    url += urldate
    url += urlSUFFIX
    SYSFILE = open(SYSOUT, 'w')
    a = time()
    url = "http://dumps.wikimedia.org/other/pageviews/" + str(YEAR) + "/" + str(YEAR) + "-" + str(
            MONTH) + "/pageviews-"
    urldate = str(YEAR) + str(MONTH) + str(DAY)
    urlSUFFIX = "-" + str(HOUR) + "0000.gz" 
    url += urldate
    url += urlSUFFIX
    try:
        COUNTFILE = urllib.request.urlretrieve(url,FILEBASE)
        b = time()
        c = b - a
        d = round(c, 3)
        SYSFILE.write("[master.py][p0_dl] Successful download of " + str(url) + " in " + str(d) + " seconds.")
    except urllib.error.HTTPError as e:
        SYSFILE.write("[master.py][p0_dl] 404 not found error")
        print(e.fp.read())

def p1_unzip_download():
    subprocess.call(['gunzip',FILEBASEGZ])
    subprocess.call(['gunzip','-S','.wikidata',FILEBASE])
    for lang in LANGLIST:
        ODIR = '/wikidata/' + str(lang) + '_ondeck'
        if not os.path.exists(ODIR):
            print("create new for " + ODIR)
            os.makedirs(ODIR)
        Fdir = '/wikidata/' + str(lang) + '_staging/'
        if not os.path.exists(Fdir):
            os.makedirs(Fdir)
    IFILE = open(FILEBASE, "r")
    for line in IFILE:
        record = line.strip().split()
        try:
            LANGUAGEUSED = record[0].strip('.').lower()
        except IndexError:
            continue
        Fdir = '/wikidata/' + str(LANGUAGEUSED) + '_staging/'
        fName = Fdir+FFILE
        try:
            fp = open(fName, "a")
        except IOError:
            if not os.path.exists(Fdir):
                os.makedirs(Fdir)
            fp = open(fName, "w")
        fp.write(str(line))
    fp.close()
    try:
        os.remove(FILEBASE)
    except OSError:
        pass

print("******DOWNLOADING Log file for Hour: "+str(HOUR)+" *******")
p0_dl()
print("******FILE Downloaded. Now unzipping and filtering by language*****")
p1_unzip_download()
print("*****UnZipped into staging area. ******")
filter_records()
print("*****DONE FIltering Records!*****")
