#/usr/bin/python
# -*- coding: utf-8 -*-
"""The master.py file downloads the hourly log file from wikipedia,
    parses it, and adds the hourly hit data to a MongoDB server instance.
    """
import os
import glob
import pymysql
import hashlib
import string
import time
import wikicount as wikicount
from tornado_mysql import pools
SYSOUT = "/wikidata/zSYSOUT"
FILEBASE = "/wikidata/staging/pagecounts.wikidata"
DAY, MONTH, YEAR, HOUR, EXPIRETIME = wikicount.fnReturnTimes()
HOUR = wikicount.minusHour(int(HOUR))
DAY, MONTH, HOUR = wikicount.fnFormatTimes(DAY, MONTH, HOUR)
LANGLIST = wikicount.LList
TOTAL_RECORDS_UPDATED = 0
from time import time
import uuid
#import pg_simple
import sys


# Connect to the database
connection = pymysql.connect(host='wikitrends-cluster.cluster-cxntgu75jukc.us-west-2.rds.amazonaws.com',
                             user='wikitrends',
                             password='wikitrends',
                             db='wikitrends',
                             charset='utf8mb4',
                             autocommit=True,
                             cursorclass=pymysql.cursors.DictCursor)

POOL = pools.Pool(
    dict(host='wikitrends-cluster.cluster-cxntgu75jukc.us-west-2.rds.amazonaws.com', port=3306, user='wikitrends', passwd='wikitrends', db='wikitrends'),
    max_idle_connections=1,
    max_recycle_sec=3) 
#conn.autocommit=True
#cur = conn.cursor()

def makeSid(lang, title,conn):
   	sid=hashlib.sha256(str(lang)+str(title)).hexdigest()
	return sid
def makeSidTotal(lang, title,hour):
   	sid=hashlib.sha256(str(lang)+str(title)+str(hour)).hexdigest()
	return sid

def update_last_hourly_hits(sid, langname, pagetitle, pagehits, hday,conn):
        pagetitle=pagetitle.decode('utf-8')
        sql = "INSERT INTO last_hourly_hits(sid,lang,title,hits,hour) VALUES('%s','%s','%s',%s,'%s') ON DUPLICATE KEY UPDATE hits=%s;" % (str(sid),str(langname),str(pagetitle),pagehits,str(hday),pagehits)
        try:
          #cursor.execute(sql)
          conn.execute(sql)
        except pymysql.err.InternalError:
          print(sql)
        except pymysql.err.ProgrammingError:
          pass
	return

def update_total_hourly_hits(sid, langname, pagetitle, hits, hday,conn):
        SUCCESSES=0
        FAILURES=0
        pagetitle=pagetitle.decode('utf-8')
   	sid=makeSidTotal(langname,pagetitle,hday)
	newsql="INSERT INTO total_hourly_hits (sid,lang,title,hits,hour) VALUES ('%s','%s','%s',%s,'%s') ON DUPLICATE KEY UPDATE hits=%s" % (str(sid),str(lang),str(title),hits,hday,hits)
        try:
          conn.execute(newsql)
          SUCCESSES+=1
        except pymysql.err.ProgrammingError:
          FAILURES+=1
	return

try:
    for lang in LANGLIST:
	count=0
        print 'now processing '+str(lang)+'...'
        for filename in glob.glob('/wikidata/' + str(lang) + '_ondeck/q*'):
		count+=1
		records=0
		ifp=open(filename,'r')
		start=time()
		if count%100==0:
		  connection.commit()
                cursor=connection.cursor()
		for line in ifp:
			line=line.strip().split()
			#en $h*!_My_Dad_Says 1 24034
			lang = line[0]
			title = line[1]
			hits = line[2]
			#size(bytes) = line[3]
			sid=makeSid(lang, title,POOL)
			update_last_hourly_hits(sid, lang, title, hits, HOUR,cursor)
			update_total_hourly_hits(sid, lang, title, hits, HOUR,cursor)
			records+=1
		end=time()
		print "Processing: Language: "+str(lang)+" Records: "+str(records)+" Total time(secs): "+str(end-start)
                #print "SUCCESSES: "+str(SUCCESSES)+" FAILURES: "+str(FAILURES)
		connection.commit()
	
		ifp.close
                os.remove(filename)		

    # connection is not autocommit by default. So you must commit to save
    # your changes.
    connection.commit()

finally:
    connection.close()


#conn.commit()
