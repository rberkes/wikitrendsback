#/usr/bin/python
# -*- coding: utf-8 -*-
"""The master.py file downloads the hourly log file from wikipedia,
    parses it, and adds the hourly hit data to a MongoDB server instance.
    """
import os
import glob
import hashlib
import string
import time
import wikicount as wikicount
from influxdb import InfluxDBClient
SYSOUT = "/wikidata/zSYSOUT"
FILEBASE = "/wikidata/staging/pagecounts.wikidata"
DAY, MONTH, YEAR, HOUR, EXPIRETIME = wikicount.fnReturnTimes()
HOUR = wikicount.minusHour(int(HOUR))
DAY, MONTH, HOUR = wikicount.fnFormatTimes(DAY, MONTH, HOUR)
LANGLIST = wikicount.LList
TOTAL_RECORDS_UPDATED = 0
from time import time
from datetime import datetime
current_time = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
import uuid
#import pg_simple
import sys


# Connect to the database
client = InfluxDBClient('127.0.0.1',8086,'root','root','wikipedia')
#conn.autocommit=True
#cur = conn.cursor()

def makeSid(lang, title):
   sid=hashlib.sha256(str(lang).encode()+str(title).encode()).hexdigest()
   return sid

def makeSidTotal(lang, title,hour):
   sid=hashlib.sha256(str(lang).encode()+str(title).encode()+str(hour).encode()).hexdigest()
   return sid

def update_last_hourly_hits(sid, langname, pagetitle, pagehits, hday,conn):
        #pagetitle=pagetitle.decode('utf-8')
        json_body = [
            {
                "measurement" : str(langname),
                "tags" : {
                     "sid" : str(sid),
                     "title" : str(pagetitle),
                     "hour" : str(hday),
                 },
                 "time" : str(current_time),
                 "fields" : { 
                     "value": hits
                  }
            }
        ]
        try:
          #cursor.execute(sql)
          conn.write_points(json_body)
        except:
          pass
        return json_body

def add_records(reclist,conn):
    for rec in reclist:
        try:
            conn.write_points(rec)
        except:
            continue
    return

for lang in LANGLIST:
    count=0
    print('now processing '+str(lang)+'...')
    for filename in glob.glob('/wikidata/' + str(lang) + '_ondeck/q*'):
        count+=1
        records=0
        ifp=open(filename,'r')
        start=time()
        reclist=[]
        for line in ifp:
            line=line.strip().split()
           #en $h*!_My_Dad_Says 1 24034
            lang = line[0]
            title = line[1]
            hits = line[2]
            #size(bytes) = line[3]
            sid=makeSid(lang, title)
            new_json=update_last_hourly_hits(sid, lang, title, hits, HOUR,client)
            reclist.append(new_json)
            if records%1000 == 0:
                add_records(reclist,client)
                reclist=[]
            records+=1
        end=time()
        try:
            print("Processing: Language: "+str(lang)+" Records: "+str(records)+" Total time(secs): "+str(end-start))
        except NameError:
            print("no records processed for: "+str(lang))
                #print "SUCCESSES: "+str(SUCCESSES)+" FAILURES: "+str(FAILURES)
    
        ifp.close
        os.remove(filename)        

    # connection is not autocommit by default. So you must commit to save
    # your changes.



#conn.commit()
