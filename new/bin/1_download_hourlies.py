#!/usr/bin/python
# coding: utf-8
"""The master.py file downloads the hourly log file from wikipedia,
    parses it, and adds the hourly hit data to a MongoDB server instance.
    """
import sys
import urllib.request, urllib.error, urllib.parse
import wikicount as wikicount
from time import time

EMAILOUT = "/wikidata/zEMAIL"
FILEBASE = "/wikidata/staging/pagecounts.wikidata.gz"
DAY, MONTH, YEAR, HOUR, expiretime = wikicount.fnReturnTimes()
HOUR = wikicount.minusHour(int(HOUR))
DAY, MONTH, HOUR = wikicount.fnFormatTimes(DAY, MONTH, HOUR)
LANGLIST = wikicount.LList
SYSOUT = '/wikidata/sysout'
FILEBASEGZ = "/wikidata/staging/pagecounts.wikidata.gz"
FILEBASE = "/wikidata/staging/pagecounts.wikidata"
FFILE = 'q_pagecounts.'+str(HOUR)


def p0_dl():
    """
    Does the dl from dumps.wikimedia.org.  
    """
    url = "http://dumps.wikimedia.org/other/pageviews/" + str(YEAR) + "/" + str(YEAR) + "-" + str(
        MONTH) + "/pageviews-"
    urldate = str(YEAR) + str(MONTH) + str(DAY)
    urlSUFFIX = "-" + str(HOUR) + "*.gz"
    url += urldate
    url += urlSUFFIX
    SYSFILE = open(SYSOUT, 'w')
    a = time()
    url = "http://dumps.wikimedia.org/other/pageviews/" + str(YEAR) + "/" + str(YEAR) + "-" + str(
            MONTH) + "/pageviews-"
    urldate = str(YEAR) + str(MONTH) + str(DAY)
    urlSUFFIX = "-" + str(HOUR) + "0000.gz" 
    url += urldate
    url += urlSUFFIX
    try:
        COUNTFILE = urllib.request.urlretrieve(url,FILEBASE)
        b = time()
        c = b - a
        d = round(c, 3)
        SYSFILE.write("[master.py][p0_dl] Successful download of " + str(url) + " in " + str(d) + " seconds.")
    except urllib.error.HTTPError as e:
        SYSFILE.write("[master.py][p0_dl] 404 not found error")
        print(e.fp.read())

print("******DOWNLOADING Log file for Hour: "+str(HOUR)+" *******")
p0_dl()
