#/usr/bin/python
# -*- coding: utf-8 -*-
"""The master.py file downloads the hourly log file from wikipedia,
    parses it, and adds the hourly hit data to a MongoDB server instance.
    """
import os
import glob
import hashlib
import string
import time
import wikicount as wikicount
from kafka import KafkaProducer
LANGUAGE_PROCESS_LIST = [ 'en','simple']

SYSOUT = "/wikidata/zSYSOUT"
FILEBASE = "/wikidata/staging/pagecounts.wikidata"
DAY, MONTH, YEAR, HOUR, EXPIRETIME = wikicount.fnReturnTimes()
HOUR = wikicount.minusHour(int(HOUR))
DAY, MONTH, HOUR = wikicount.fnFormatTimes(DAY, MONTH, HOUR)
LANGLIST = wikicount.LList
TOTAL_RECORDS_UPDATED = 0
RECDIVIDE = 1000
from time import time
from datetime import datetime
current_time = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
import uuid
import sys
import io
producer = KafkaProducer()
HOMEDIR=os.path.expanduser('~')
WD=HOMEDIR+'/Dropbox/Bitbucket/wikitrendsback/new/bin/'
schema_path = WD+"record.avro"
#schema = avro.schema.Parse(open(schema_path,"r").read())

def makeSid(lang, title):
   sid=hashlib.sha256(str(lang).encode()+str(title).encode()).hexdigest()
   return sid
def makeSidTotal(lang, title,hour):
   sid=hashlib.sha256(str(lang).encode()+str(title).encode()+str(hour).encode()).hexdigest()
   return sid
def add_records(langname,reclist,producer):
   for rec in reclist:
       #writer=avro.io.DatumWriter(schema)
       #bytes_writer=io.BytesIO()
       #encoder=avro.io.BinaryEncoder(bytes_writer)
       #writer.write(rec,encoder)
       #raw_bytes=bytes_writer.getvalue()
       #print(str(rec).encode('utf-8'))
       producer.send(str(langname),str(rec).encode('utf-8'))
       #producer.send(str(langname),raw_bytes)
   producer.flush()
   return
def update_last_hourly_hits(sid, langname, pagetitle, pagehits, hday,conn):
        str_rec = str(pagetitle)+'#'+str(sid)+'#'+str(hday)+'#'+str(hits)
        return str_rec

for lang in LANGLIST:
    records=0
    count=0
    print('now processing '+str(lang)+'...')
    for filename in glob.glob('/wikidata/' + str(lang) + '_ondeck/q_p*'):
      if lang in LANGUAGE_PROCESS_LIST:
        count+=1
        records=0
        ifp=open(filename,'r')
        start=time()
        reclist = []
        for line in ifp:
            line=line.strip().split()
           #en $h*!_My_Dad_Says 1 24034
            lang = line[0]
            title = line[1]
            hits = line[2]
            sid=makeSid(lang, title)
            new_json=update_last_hourly_hits(sid, lang, title, hits, HOUR,producer)
            reclist.append(new_json)
            if records % RECDIVIDE == 0:
                add_records(lang,reclist,producer)
                reclist=[]
                print('+'+str(RECDIVIDE)+' records added! '+str(records)+' total...')
            records+=1
        add_records(lang,reclist,producer)
        end=time()
        try:
            print("Processing: Language: "+str(lang)+" Records: "+str(records)+" Total time(secs): "+str(end-start))
        except:
            print("error processing: "+str(lang))
    
        ifp.close()
        os.remove(filename)
      else:
        os.remove(filename)     




