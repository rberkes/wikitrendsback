import threading
import time
from memsql.common import database
import json
import datetime

TODAY=datetime.datetime.today().strftime('%Y-%m-%d-%H')
HOST="10.138.0.2"
PORT = 3306
USER = "wikicount"
PASSWORD = "wikicount"
DATABASE = "en"

def get_connection(db=DATABASE):
    """ Returns a new connection to the database. """
    return database.connect(host=HOST, port=PORT, user=USER, password=PASSWORD, database=db)

ofp=open('/home/rob_berkes/wikitrendsback/new/reports/toparticles.'+str(TODAY),'w')

PROCESSED_LANGUAGES=['en','simple']
for LANG in PROCESSED_LANGUAGES:
    RFNAME='toparticles.'+str(LANG)+'.'+str(TODAY)
    rfp = open('/home/rob_berkes/wikitrendsback/new/renders/'+str(LANG),'w')
    QUERY_TEXT = "SELECT title, sum(hits) as Hits from "+str(LANG)+" group by sid order by Hits desc limit 50;"
    with get_connection(db='wikitrends') as conn:
        RETLIST=conn.query(QUERY_TEXT)
    ofp.write("Top 50 "+str(LANG)+" Articles\n")
    ofp.write("=======================\n")
    Counter=1
    for line in RETLIST:
        ofp.write(str(Counter)+') '+line['title']+' '+str(line['Hits'])+'\n')
        rfp.write(str(Counter)+'#'+str(line['title'])+'#'+str(line['Hits'])+'#\n')
        Counter+=1
    rfp.close()
ofp.close()
