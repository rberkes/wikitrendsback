#!/usr/bin/python
# coding: utf-8
import os
import gzip
import subprocess
import wikicount as wikicount
import glob

FILEBASEGZ = "/wikidata/staging/pagecounts.wikidata.gz"
FILEBASE = "/wikidata/staging/pagecounts.wikidata"
DAY, MONTH, YEAR, HOUR, EXPIRETIME = wikicount.fnReturnTimes()
HOUR = wikicount.minusHour(int(HOUR))
DAY, MONTH, HOUR = wikicount.fnFormatTimes(DAY, MONTH, HOUR)
LANGLIST = wikicount.LList
TOTAL_RECORDS_UPDATED = 0
STRIPLIST = ["Special:", "Template:", "Wikipedia_Talk:", "Файл:", "Категория:", 
	     "Category_talk:", "Talk:", "Image:", ".php"]
FFILE = 'q_pagecounts.'+str(HOUR)

subprocess.call(['gunzip','-S','.wikidata',FILEBASE])
for lang in LANGLIST:
    ODIR = '/wikidata/' + str(lang) + '_ondeck'
    if not os.path.exists(ODIR):
        print("create new for " + ODIR)
        os.makedirs(ODIR)
    Fdir = '/wikidata/' + str(lang) + '_staging/'
    if not os.path.exists(Fdir):
        os.makedirs(Fdir)

for files in glob.glob('/wikidata/staging/*'):
    IFILE = open(files, "r")
    try:
        for line in IFILE:
            record = line.strip().split()
            try:
                LANGUAGEUSED = record[0].strip('.').lower()
            except IndexError:
                continue
            Fdir = '/wikidata/' + str(LANGUAGEUSED) + '_staging/'
            fName = Fdir+FFILE
            try:
                fp = open(fName, "a")
            except IOError:
                if not os.path.exists(Fdir):
                    os.makedirs(Fdir)
                fp = open(fName, "w")
            fp.write(str(line))
        fp.close()
    except UnicodeDecodeError:
        continue
    

    try:
        os.remove(FILEBASE)
    except OSError:
        pass




