import psycopg2

conn=psycopg2.connect("dbname=wikitrends user=wikitrends")
cur = conn.cursor()

cur.execute("CREATE TABLE last_hourly_hits (id uuid, language varchar, title text, hits integer, hour smallint, constraint lastIdHour PRIMARY KEY(id, hour));")
cur.execute("ALTER TABLE pageinfo ADD titlehash text;")
conn.commit()
cur.close()
conn.close()


