import pymysql
connection = pymysql.connect(host='wikitrends-cluster.cluster-cxntgu75jukc.us-west-2.rds.amazonaws.com',
                             user='wikitrends',
                             password='wikitrends',
                             db='wikitrends',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

cursor = connection.cursor()
#sql = "DROP TABLE last_hourly_hits;"
#cursor.execute(sql)
sql = "DROP TABLE total_hourly_hits;"
cursor.execute(sql)
sql = "CREATE TABLE last_hourly_hits (sid VARCHAR(60),lang VARCHAR(3), title VARCHAR(30), hits DOUBLE, hour SMALLINT)"
cursor.execute(sql)
sql = "CREATE TABLE total_hourly_hits (sid VARCHAR(60),lang VARCHAR(3), title VARCHAR(30), hits DOUBLE, hour SMALLINT)"
cursor.execute(sql)
