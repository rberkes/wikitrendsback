#/usr/bin/python
# coding: utf-8
"""The master.py file downloads the hourly log file from wikipedia,
    parses it, and adds the hourly hit data to a MongoDB server instance.
    """
import os
import glob
import psycopg2
import hashlib
import string
import time
import wikicount as wikicount
SYSOUT = "/wikidata/zSYSOUT"
FILEBASE = "/wikidata/staging/pagecounts.wikidata"
DAY, MONTH, YEAR, HOUR, EXPIRETIME = wikicount.fnReturnTimes()
HOUR = wikicount.minusHour(int(HOUR))
DAY, MONTH, HOUR = wikicount.fnFormatTimes(DAY, MONTH, HOUR)
LANGLIST = wikicount.LList
TOTAL_RECORDS_UPDATED = 0
from time import time
import uuid 
conn = psycopg2.connect('dbname=wikitrends ')
conn.autocommit=True
cur = conn.cursor()
PAGESFILE='/wikidata/pages.csv'
TOTALHOURLYFILE='/wikidata/totalhour.csv'
LASTHOURLYFILE='/wikidata/lasthourly.csv'
TOTALHOURLYP=open(TOTALHOURLYFILE,'w')
LASTHOURLYP = open(LASTHOURLYFILE,'w')
PAGESFILEP = open(PAGESFILE,'w')

def check_pageinfo(lang, title):
   	shahash=hashlib.sha256(title).hexdigest()
	cur.execute('SELECT id FROM pageinfo WHERE language = %s and titlehash = %s', (lang, shahash))
	row = cur.fetchone()
	if row == None:
		sid = uuid.uuid4()
	#	cur.execute('INSERT INTO pageinfo(id,title,titlehash,language) VALUES(%s, %s, %s, %s)', (str(sid), title, shahash, lang))
		PAGESFILEP.write(str(sid)+','+str(title)+','+str(shahash)+','+str(lang)+'\n')
	else:
		sid = row[0]
#	conn.commit()
	return sid

def update_last_hourly_hits(sid, langname, pagetitle, pagehits, hday):
#	cur.execute("SELECT * FROM last_hourly_hits WHERE hour = %s AND id = %s",(hday,str(sid)))
#	row=cur.fetchone()
#	if row == None:
#		cur.execute("INSERT INTO last_hourly_hits(id,language,title,hour,hits) VALUES (%s, %s, %s, %s, %s)",(str(sid), langname, pagetitle, hday, pagehits))
#	else:
#		cur.execute("UPDATE last_hourly_hits SET hits = %s WHERE id = %s",(pagehits, sid))
#	conn.commit()
	LASTHOURLYP.write(str(sid)+','+str(langname)+','+str(pagetitle)+','+str(pagehits)+','+str(hday)+'\n')
	return

def update_total_hourly_hits(sid, langname, pagetitle, hits, hday):
	cur.execute("SELECT hits FROM total_hourly_hits WHERE hour = %s AND id = %s", (hday,str(sid)))
	row=cur.fetchone()
	if row == None:
		TOTALHOURLYP.write(str(sid)+','+str(langname)+','+str(pagetitle)+','+str(hday)+','+str(hits)+'\n')
#		cur.execute("INSERT INTO total_hourly_hits(id,language,title,hour,hits) VALUES (%s, %s, %s, %s, %s)",(str(sid),langname,pagetitle,hday,hits))
	else:	
		total=row[0]
		newtotal=int(total)+int(hits)
		cur.execute("UPDATE total_hourly_hits SET hits = %s WHERE id = %s",(newtotal,sid))
#	conn.commit()
	return
def copy_last_hourly_file():
	copy_start=time()
	print "\nstarting copy of last hourly at "+str(copy_start)
	cur.execute("COPY last_hourly_hits(id, language, title, hour, hits) FROM '{0}'".format(LASTHOURLYFILE))
	copy_end=time()
	print "\ncopy done and took "+str(copy_end-copy_start)+" seconds."
	return
	
def copy_pagesfile():
	copy_start=time()
	print "\nstarting copy of pages file at "+str(copy_start)
	cur.execute("COPY pageinfo(id, title, titlehash, language) FROM '{0}'".format(PAGESFILE))
	copy_end=time()
	print "\ncopy done and took "+str(copy_end-copy_start)+" seconds."
	return
def copy_total_hourly_file():
	copy_start=time()
	print "\nstarting copy of total hourly at "+str(copy_start)
	cur.execute("COPY total_hourly_hits(id, language, title, hour, hits) FROM '{0}'".format(TOTALHOURLYFILE))
	copy_end=time()
	print "\ncopy done and took "+str(copy_end-copy_start)+" seconds."
	return



copy_pagesfile()
copy_last_hourly_file()
copy_total_hourly_file()
