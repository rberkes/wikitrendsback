import psycopg2
conn=psycopg2.connect('dbname=wikitrends')
cur=conn.cursor()

cur.execute("DELETE FROM pageinfo;")
cur.execute("DELETE FROM last_hourly_hits;")
cur.execute("DELETE FROM total_hourly_hits;")
conn.commit()
cur.close()
conn.close()
