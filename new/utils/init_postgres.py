import psycopg2
#conn=psycopg2.connect("dbname=postgres user=postgres")
#cur = conn.cursor()
##cur.execute("CREATE DATABASE wikitrends WITH OWNER wikitrends;")
##presently, 'cannot be run in a transaction block' meaning must be done manually, as below:
#bash-4.3$ psql 
#could not change directory to "/home/rob/Dropbox/Bitbucket/wikitrendsback/new": Permission denied
#psql (9.4.4)
#Type "help" for help.

#postgres=# CREATE ROLE wikitrends;
#CREATE ROLE
#postgres=# CREATE DATABASE wikitrends WITH OWNER wikitrends;
#CREATE DATABASE
#postgres=# ALTER ROLE wikitrends WITH LOGIN;
#ALTER ROLE
#postgres=# \q


conn=psycopg2.connect("dbname=wikitrends user=wikitrends")
cur = conn.cursor()

cur.execute("CREATE TABLE total_hourly_hits (id uuid, language varchar, title text, hits integer, hour smallint, constraint idHour PRIMARY KEY(id, hour));")
cur.execute("CREATE TABLE pageinfo (id uuid, language varchar, title text, bayesian text, url text);")   
conn.commit()
cur.close()
conn.close()


